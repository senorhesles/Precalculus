
-- ordPair :: (Double -> Double) -> [Double] -> [(Double,Double)]
ordPair f xs = zip xs (map f xs)


fun1 x = x^2 + (3*x)

nats :: [Double]
nats = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]

near :: [Double]
near = [-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,
       0.7,0.8,0.9,1.0]

{-
Sep 4

Combinations of functions:

Addition:
(f + g)(x) = f(x) + g(x)
domain: intersection of f and g

example: y = x^2; z = e^x; (f+g)(x) = x^2 + e^x

----------
Difference:
(f - g)(x) = f(x) - g(x)
zeros of (f-g)(x) are intersections of  f and g

To show that two things are equal, you show that they have no difference.

-----------

Composition:
The composition of f(x) and g(x) is (f o g) (x) = f(g(x))

------------------------
How to represent Functions:

relationship between sets
missiles -> targers
input -> output
independent -> dependent

Four Representations:
verbally
equations -> explicitly, implicitly, parametrically (Explicitly: y = x^2; Implicitly: x^2 - y - 2 = 0 (explicit is possible), x^2 - y^2 - 2 = 0


Parametric equations:

y = (x-2)^2 + 1

Lets just say..."x = t + 2" and "y = t^2 + 1"; in this case 't' is called the parameter...translate "x = t + 2" as "t = x - 2"

x^2 + y^2 = 1 (Unit Circle) Its not an explicit function. However any point ....if its in first quadrant, some point t, is going to be (cos(t),sin(t))

(cos(t),sin(t))
x = cos(t)
y = sin(t)
0 <= t <= 2pi

t | x | y
0 | 1 | 0
pi/2 | 0 | 1

1 revolution is 10sec
(2pi radians/10 seconds) = pi/5 * rad/s
x = cos(pi/5 * t)
y = sin(pi/5 * t)
0 <= t <= 10sec = one trip counterclockwise, starting at point (1,0)

To double speed, you'd do 2pi/5seconds rads/s

x = sin(t)
y = cos(t)
0 <= t <= 2pi

t|x|y
0|0|1
pi/2|1|0


-----------------------------------
Inverse:

If set 'A' is related to set 'B'
and a point 'a' is related to
a point 'b', then the inverse relation relateds b to a

Relation: {(1,a),(1,b),(2,c),(3,a),(4,d)}
Inverse: {(a,1),(b,1),(c,2),(a,3),(d,4)}

Graphs: the inverse of a frelation is the mirror image in the line y = x.

The inverse of a function y = f(x)
is the function y = f^-1(x) defined by
these two equations:

f^-1(f(x)) = x for all x in the domain of f
and
f(f^-1(x)) = x for all x in the domain of f^-1


Are there any functions where there are two inverses? where x -> f(x); f^-1(f(x)) = y

Ex: if A = f(t) gives the value in dollars of an investment after t years

With injective functions, and bijective functions, you havea guarantee, that the inverses are going to functions...whereas if its only surjective, then there's no guarantee

Ex:
f(x) = x^2
f^-1(x) = sqrt(x)

f(-2) = 4; f^-1(4) = 2

y = sqrt(x) is not hte inverse of y = x^2

All of the inverse functions of the trig functions...you need to restrict the domains.

A function f(x) is bijective if given any numbers 'a' and 'b' in the domain of f(a) = f(b), then a = b.

----------------
If f(x) is bijective, either on its own or on a restricted domain,
then f(f^-1(x)) = x, will help you find an equation for f^-1(x)

________________
If y = f^-1(x), then f(y) = x.

Example
f(x) = 3x - 1
f(y) = x
x = 3y - 1
x + 1 = 3y
y = (x + 1)/3
y = 1/3(x+1) = (1/3)x + (1/3)

--------------------
To verify that these are inverses you need to compose them.

f(x) = 3x - 1
f^-1(x) = 1/3(x) + 1/3

f^-1(f(x)) = 1/3(f(x) + 1/3 = 1/3(3x -1) + 1/3 = 1/3 * 3x - (1/3) + 1/3 = x
f(f^-1(x)) = 3f^01(x) - 1 = 3(1/3(x) + (1/3)) - 1 = x + 1 - 1 = x

-------------------------
Example
f(x) = (x + 3)/(2x - 1); Find f^-1(x)
y = (x + 3)/(2x - 1)
x = (y + 3)/(2y - 1)
x(2y - 1) = y + 3
2xy - x = y + 3
2xy - y = x + 3
y(2x - 1) = x + 3
y = f^-1(x) = (x+3)/(2x - 1)

f(f^-1(x)) = ((x+3)/(2x-1)) + 3 / 2((x+1)/(2x-1)) - 1 (multiply by common denominators) * (2x-1)/(2x-1)

=

(x+3) + 3(2x-1) /

------------
Example:
g(x) = (2x+1)/(x-5)
y = (2x + 1)/(x - 5)
x = (2y + 1)/(y -5); x(y-5) = 2y + 1; xy - 5x = 2y + 1; xy - 2y = 5x + 1; y(x-2) = 5x+1; y = (5x+1)/(x-2)


----------------

example:
f(x) = (x+1)/x
g(x) = 1/(x-1)

f . g = ((1/(x-1)) + 1)/(1/(x-1)) * (x-1)/(x-1) = (1+(x-1))/1 = x

g . f ...

example:
f(x) = x+1 / x
Find f^-1(x):
y = (x+1)/x
x = y + 1 / y
xy = y + 1
xy - y = 1
y(x - 1) = 1
x = 1/(x -1)

example:
g(x) = (x+1)/(x-3)
x = (y + 1)/(y -3)
x(y-3) = y+1
xy - 3x = y + 1
xy - y = 3x + 1
y(x-1) = 3x+1
y = f^-1(x) = 3x+1 / x-1

example:

a = 3t+1 / t-5
t = 3a+1 / a-5


CHAPTER 1.6
-----------

Horizontal shifts

y = f(x - c) shifts horizontally
right if c > 0 or left if c < 0

ex: f(x) = x^2
    y = f(x-3) = (x-3)^2


-----------
Vertical shifts

y = f(x) + c (shifts up if c > 0; down if c < 0)

-----------
Vertical stretches and compressions
-----------
y = c * f(x) stretches if c > 1; compresses if 0 < c < 1

g(x) = abs(x)
y = 2 * g(x) = 2 * |x|

-----------
horizontal stretch or compression
-----------

y = f(cx): Stretch when 0 < c < 1; compression when c > 1



-----------
Reflections in x axis
-----------

y = - f(x) reflects y = f(x) in the x axis

-----------
Reflections in y axis
-----------
y = f(-x) is hte mirror image of y = f(x) in the y axis

y = |f(x)| = |x^2 - 5|

y = f(abs(x)) = { f(x) if x => 0 q1 and q4 ; f(-x) if x < 0

y = abs(abs(x) -2) -2

51. y = 2 + 3(f(x+1)); left 1; vert stretch by 3; vert shift 2 up

number 31
10 percent bleach = .10x
45 percent bleach = .45y = .45 * (100-x)
25 percent bleach = .25(100) = 25

.10x + .45(100 - x) = 25
.10x + 45 - .45x = 25
-.35x = -10
x = 28.6 gallons

-------------
Polynomial Function
-------------
exponents have to be positive integers
f(x) = An*X^n + A(n-1)*X^(n-1) + ... + A1x + A0
        term      term	       	       term  term

where A0, A1,...,An are real numbers (coefficients)
and n is a non-negative integer
A0 is the constant term
n is the degree

Example:
f(x) = -3x^7 - 14x^3 + 2x - 20
     = -3x^7 + 0x^6 + 0x^5 + 0x^4 - 14x^3 + 0x^2 + 2x^1 -20
     = A7x^7 + a6x^6 + a5x^5 + ....



-----------
Polynomials of Degree Zero
-----------
f(x) = a0 'is a constant function'
y = pi
y = 5
graph is a horizontal line

-------------
Polynominals of Degree One
------------
f(x) = a1x + a0 (a1 =/= 0)
example: y = 3x - 2
	 y = -12x + 5

Graphs are lines
slope is a1
y intercept a0

------------
Linear Function
------------
f(x) = mx + b

m = slope = rise / run = delta y / delta x = (y2 - y1) / (x2 - x1) = (f(x2) - f(x1)) / (x2 - x1)

b = f(0); "It represents an initial value or a fixed cost."

Ex. Find an equation for the linear function f(x) where

f(-3) = 5 and
f(4) = -10

m = (-10 - 5) / (4 - -3) = (-15/7) = -2(1/7)
f(x) = -15/7x + b
-10 = -15/7(4) + b
-10 = -60/7 + b
b = -60/7 + 70/7 = 10/7
f(x) = -15/7x - 10/7 (y intercept form)

----------------
Direct proportion where y is directly proportional to x
---------------
g(x) = f(x - xb) + y0
g(x) = m(x - x0) + y0 (point slope form) !!!
y = -15/7(x-4) - 10

where x0 is displacement from the origin on the x
      y0 is displacement from the origin on the y
      (x0,y0) is ordered pair of other function



-------------------
Degree 2 polynomials
-------------------
f(x) = a2x^2 + a1x + a0 (a2 =/= 0); i.e. Quadratic function

Ex. f(x) = x2 (leading coefficient of 2, all others are 0) (simplest quadratic)

Ex.
Find an equation for this function:

vertex has 1,3
y = a * f(x-1) + 3 where (a < 0)

when x = 0, y = 1
1 = a(0 - 1)^2 + 3
1 = a(1) + 3
-2 = a
y = -2(x - 1)^2 + 3 (vertex form)
y = -2(x-1)^2 + 3
  = -2(X^2 -2X +3) +3
  = -2x^2 + 4x - 2 + 3
  = -2x^2 + 4x + 1 

Quadratic Functions
f(x) = ax^2 + bx + c (a =/= 0)
Standard form / General form

Vertext form
f(x) = a(x-h)^2 + k

f(x) = a(x - h)^2 + k
     = a(x^2 - 2xh + h^2) + k
     = ax^2 - 2ahx + ah^2 +k
     = ax^2 + bx + c
     b = -2ah; c = ah^2 + k
     h = - b/2a (x-coord of vertex)
     f(h) = k = (y-coord of vertex)


A baseball is popped straight into the air. It's height in feet as a function of time t in seconds is h = f(t) = -16t^2 + 48t + 4
(It's a parabola because its quadratic)

time for the maximum height (i.e. the x coord) is h = -b/2a = -48/2(-16) = 3/2 = 1.5s
f(3/2) = -16(3/2)^2 + 48(3/2) + 4
       = -36 + 72 + 4
       = 40 ft

f(0) = -16t^2 + 48t + 4 = 0
     = -4(

x = (-b +- sqrt(b2 - 4ac)) / 2a

h = 3/2, k = 40, a = -16
f(t) = -16(t - 3/2)^2 +40
0 = -16(t - 3/2)^2 + 40
-40 = -16(t - 3/2)^2
5/2 = (t - 3/2)^2
t - 3/2 = +-sqrt(5/2)
t = 3/2 + sqrt(5/2) (because time can't be negative)

(you could complete the square...but you already have h and k...so there's no point)

------------------
9/17/15
Example of solving stuff in vertex form

Two points vertex(1,-3) (2,5)

f(x) = a(x - h)^2 + k
h = 1, k = -3
need to find a
f(x) = a(x-3)^2 - 3
f(2) = 5
5 = a(2 - 1)^2 - 3
8 = a(1)

f(x) = 8(x-1)^2 - 3
y intercept 8(-1)^2 -3 = 5
8(x -1)^2 - 3 = 0
8(x-1)^2 = 3
(x-1)^2 = 3/8
x - 1= +-sqrt(3/8)
x = 1 +- sqrt(3/8)

x = -(-16) +- sqrt((-16)^2 - 4*8*5) / 2(8)

x = 16 +- sqrt(16)

---------------
What hapens in the long run?
---------------
It ends up in quadrants of 1 and 2, so, large and negative values of x

As x -> oo
   y -> +oo

as x -> -oo
   y -> +oo

lim  	y = oo
x -> oo

lim	y = -oo
x -> -oo



----------------------
If you compute delta y / delta x, for two given points, it will give you the average rate of change over some interval


Take two ordered pairs for a nonlinear function. Compute their slope. Is that number equal to the average of the slopes of all the ordered pairs in the interval?

-------------------------
Section 2.2 Power functions
-------------------------
Identify by an equation what the power function looks like

f(x) = k*x^a
k, a constant

k /= 0

------------------
Monomials
f(x) = k * x^n
n is a positive integer

(a polynomial is just the sum of a bunch of monomials


---------------
Negative exponent powers
-----------------
y = x^-1 = 1/x (forms a hyperbola)
y = x^-2 = 1/x^2


-----------------------
Fractional exponent powers
------------------------
y = x^(1/2) = sqrt(x)


y = x^1/3 = 3sqrt(x)

y = x^2/3 = 3sqrt(x^2) (has a "cusp")



f(x) = anx^n + an-1x^n-1 +
       leading
       term
       controls the end behavior

x = 1000
n = 6
x^n = (1000)^6 = 10^18
x^n-1 = (1000)^5 = 10^15

The end behavior of a polynomial is the same as the end behavior of the first monomial.

The end behavior if n is odd, and an > 0
lim 	f(x) = oo
x -> oo
	f(x) = -00
lim x -> -oo


The end behavior if n is even, and an > 0


lim 	f(x) = oo
x -> 00

lim	f(x) = 00
x -> -oo

To find x-intercepts, factor
f(x) = 8x^2 - 18x - 5
"The ac method"
8 * -5 = -40
(-20)(2) = -40
-20 + 2 = -18
-18x = -20x + 2x
8x^2 -20x +2x - 5
= 4x(2x-5) + (2x-5)
= (4x+1)(2x-5)

Example:
number 15 on 2.3 (Finding zeros by factoring by grouping)
f(x) = 2x^3 + 3x^2 -6x - 9
     = x^2(2x+3) - 3(2x+3)
     =(x^2 - 3)(2x + 3)
     =(x-sqrt(3))(x+sqrt(3))(2x+3)
     zeros: sqrt(3),-sqrt(3),(-3/2)

Find an equation with x intercepts (+sqrt(5),0) (-sqrt(5),0), and (4,0)
and
y-intercepts of (0,10)
Factors: (x-sqrt(5))(x+sqrt(5))(x-4)
y = a(x-sqrt(5))(x+sqrt(5))(x-4)
y = a(x^2 -5)(x-4)

10 = a(0^2-5)(0-4)
10 = 20a
a= 1/2
f(x) = 1/2(x^2 -5)(x-4)

y = a(x^2 -5)(x-4)...^2...gives a bounce at the end (multiplicity refers to adding the ^2 at the end)
10 = a(-5)(-4)^2
10 = -80a
a = -1/8

------------------------
2.4
------------------------

We're trying to get to Remainder Theorem and Factor Theorem

We already know that if (x - a) is a factor of a polynomial f(x),
then 'x = a' is a zero (i.e. an x-intercept). f(a) = 0


If (x-a) is a factor, then you can do this
f(x) = (x-a)*g(x)

So f(a) = (a-a)(g(a)) = 0

If you have a zero, then you have a factor.

Factor theorem!!!
If "f(a) = 0" where "f(x) is a polynomial"
then "x-a" is a factor of "f(x)"

--------------------
Dividing polynomials
--------------------
Ex. Find the quotient and remainder when "f(x) = x^3 - 3x^2 + x - 1" is divided by "x^2 + 1".


Long division:	  quotient remainder
                  208 and 8/18
3752 / 18 = 18 | 3752
       	       	 36
		  152
		  144
		    8

         x   - 3
x^2 + 1 |x^3 - 3x^2 + x - 1
       -(x^3        + x)
       	     - 3x^2     - 1
	   -(- 3x^2     - 3)
	       		  2

quotient: (x - 3)
remainder: 2

(x^3 - 3x^2 + x - 1) / (x^2 + 1) = (x-3) + 2/(x^2 + 1)
or
x^3 - 3x^2 + x - 1 = (x-3)(x^2 + 1) + 2
f(x)
Synthetic division!!!

The division algorithm for polynomials!!!
If a polynomial "f(x)" is divided by a polynomial "g(x)",
then "f(x) = q(x)*g(x) + r(x)"
where "q(x)" is the quotient (polynomial)
      "r(x)" is the remainder (polynomial)
      and the degree of "r" is strictly less than the degree of "g"


Special case: g(x) = x - a
f(x) = q(x)(x-a) + r where r is a constant (i.e. degree of r is zero)

f(a) = q(a)(a-a) + r = r

Ex: x^3 - 3x^2 + x - 1
a = -1
x + 1 = x - (-1)


    x^2 - 4x   + 5
x | x^3 - 3x^2 + x - 1
  -(x^3 +  x^2)
  	- 4x^2 + x - 1
       -(-4x^2 - 4x)
       	         5x - 1
	       -(5x + 5)
	       	     -6

x^3 - 3x^2 + x - 1 = (x + 1)(x^2 - 4x + 5) - 6

Synthetic division!!! (optional)
Only applies when dividing by x - a
-1 | 1 -3 1 -1 (a = -1)
       -1 4 -5
     1 -4 5 (-6) remainder
     x^2 -4x + 5


Remainder Theorem!!!
If a polynomial f(x) is divided by a polynomial (x-a)
the remainder is f(a). All polynomials are continuous.

Ex Find k such that is 9x^5 - kx^4 + 2kx - 7
is divided by x - 1, the remainder is 2

1 | 9 -k 0 0 2k -4
      9
    9 -k+9	 2

r = 2 = f(a) = f(1) = 9 - k + 2k - 7
2 = 2 + k
k = 0

Find k such that when...

Example:
Is x + 3 a factor of x^3 -16x^2 + x - 32
       	   	     -27 -16(9) - 3 - 32
		     	 -144
			 -
			 -84 -32
			 -126

Factor theorem!!!
"(x-a)" is a factor of "f(x)" iff
"a is a zero" meaning "f(a) = 0"

Example:
For what values of n is x-1 a factor of "x^n -1"
Remainder = f(1) = 1^n - 1 = 0 for all n.

For all n, x-1 is a factor of "x^n - 1"

For what values of n is x + 1 a factor of x^n + 1
a = -1
(-1)^n + 1 = 0
Where n is odd.

Example:
Completely factor (Write as a product of polynomials of the lowest degree)
3x^3 + 4x^2 - 5x - 2

1 is a zero, because 3 + 4 - 5 - 2 = 0
If you don't see it, you can graph it
nn
Reduce by dividing by (x - 1)

1 | 3 4 -5 -2
      3  7  2
    3 7  2  0

3x^2 + 7x + 2
x = -2

(x-1)(x-2)(3x+1)

If you had to solve x^2 - x + 1 = 0

x = 1 +- sqrt((-1)^2 - 4*1*1) / 2(1)
  = 1 +- sqrt(-3) / 2

no real zeros ... no factors


factors of primes is the same as obtaining the factors of irreducible polynomials

x^2 - x + 1

Completely factor:
x^3 - 3x^2 + 3x - 1

1 is a zero, 1 - 3 + 3 - 1 = 0
(x - 1)

1 | 1 - 3 + 3 - 1
        1 - 2   1
    1 - 2   1   0

x^2 - 2x + 1
(x-1)(x-1)(x-1)
(x-1)^3

JMJ

Completely factor
x^3 + 3x^2 - 3x - 1
1 is a zero

(x-1)

1 | 1 3 - 3 - 1
      1   4   1
    1 4   1   0

(x-1)(x^2 + 4x + 1)
(x-1)(x^2 + 4x + 4)
x = -2 +- sqrt(3)
(x -(-2+sqrt(3)))

(x - 1)(x+2 +sqrt(3))(x+2-sqrt(3))

Given degree and zeroes, find equation

Degree 3, leading coefficient of 2, zeros of 2 1/2 3/2

y = 1/2(x-2)(2x-1)(2x-3)

y = a(x-2)(x-1/2)(x-3/2); when y intercept is 10

10 = a(0-2)(0-1/2)(0-3/2)
a = -20/3

Ex.
zeros: 2, 1/2, 3/2
lim f(x) = oo
x->oo

lim f(x) = oo
x-> -oo

and y intercept of 10

x^2 + 4x + 1 = 0
x^2 + 4x _ = -1
x^2 + 4x + 4 = 3
(x+2)^2 = 3
x+2 = +-sqrt(3)
x = -2 +-sqrt(3)

(2+sqrt(3)) + (2-sqrt(3)) = 4
(2+sqrt(3))(2-sqrt(3)) = 4 - 3 = 1

3x^2 - 2x - 4 = 0
x^2 - 2/3x - 4/3 = 0
x^2 - 2/3x + 1/9 = 4/3 + 1/9
(x - 1/3)^2 = 13/9
x - 1/3 = (+-sqrt(13))/3
x = 1/3 +- (+-sqrt(13))/3

y = x^2 + 4x + 1
h = -b/2a = -4/2 = -2
k = (-2)^2 + 4(-2) + 1


Average rate of change!!!
delta y / delta x

(y2 - y1) / (x2 - x1)


JMJ
-}

{- Rational function is one where you can write stuff like
f(x) = a(x)/b(x)
where a(x) and b(x) are polynomials
and the degree of b(x) is at least 1
-}

fun2 x = x/x
--technically y = 1 is a horizontal asymptote, has a hole/removable discontinuity
-- at 0


fun3 x = x^2 / x -- this one has a slant asymptote

fun4 x = 1/x -- no matter how large x is, y never reaches 0; hyperbola;
         -- horiz asymp at y = 0; vert asymp at x = 0

{-
Def: A function f(x) has a horizontal asymptote of y = b
if lim x->oo f(x) = b, or lim x ->-oo f(x) = b (as x increases without bound)

Def: A function f(x) has a vertical asymptote of x = a
if lim x->a+ f(x) = oo
or if lim x ->a- f(x) = oo
or if lim x->a+ f(x) = -oo
or if lim x->a- f(x) = -oo
-}
fun5 :: Double -> Double
fun5 x = 1/(x^2)
-- horizontal asymptote of y = 0
-- vertical asymptote of x = 0
fun6 :: Double -> Double
fun6 x = (x-1)/(x-1) -- hole at x = 1; no vertical asymptote

fun7 :: Double -> Double
fun7 x = ((x-1)*(x-3))/((x-1)*(x-5)) -- hole at x = 1; vert asymp x = 5; yint x=3
-- lim x-> 1; y = 1/2

fun8 :: Double -> Double
fun8 x = (x-1)/(x-1)^2

fun8a :: Double -> Double
fun8a x = 1/(x-1)



-- To find vertical asymptotes, your looking at both, if denominator is bigger

{-
How to find horizontal asymptotes

Easiest case is when the denominator is bigger than the numerator.
-}

fun9 x = 1/x -- horasy y = 0

fun10 x = 1/(x^2) -- horasy y = 0

fun11 x = 1/(x-1) -- horasy y = 0

fun12 :: Double -> Double
fun12 x = (x+5)/((x-1)*(x+3))

fun13 :: Double -> Double
fun13 x = (3*x^3 - 2*x^2 + 4*x - 17)/(-5*x^2 + 14*x + 3)
{-

as abs(x) -> oo, y -> anX^n / amX^m
in
anX^n + an-1*x^(n-1)+...a1x + a0
/
bmX^m + b






--eqq :: (Double -> Double)
-}

fun14 :: Double -> Double
fun14 x = (-5*x^2 + 14*x +3)/(3*x^3 - 7*x^2 + 4*x - 17)

fun15 :: Double -> Double
fun15 x = (-5*x^3 + 14*x + 3)/(3*x^3 - 4*x^2 + 300) -- > -5x^3/3x^3 > -5/3
-- horiz asymptote y = -5/3

fun16 :: Double -> Double
fun16 x = (x^100 + 1)/ -- leading term x^100
          (2*x - 5)^100 -- leading term 2^100
          -- horiz asymptote y = 1/(2^100)

{-
f(x) = 2x^2 + 1 / x - 3
For large x, f(x) ~ 2x^2 / x = 2x

use long division

2x^2 + 1 / x - 3 = (2x + 6) + 19/(x-3)
                   slant asymptote


Inequalities: <, <=, >, >=

Solve: 2x - 3 >= 0; don't multiply by variables, because variables could be negative
       2x >= 3
       x >= 3/2
       [(3/2),oo)

       -2x - 3 >= 0
       -2x >= 3
       x <= -3/2
       (-oo, (-3/2)]


Solve: 2x^2 - 1 >= 3x
       2x^2 - 3x - 1 >= 0 -- find the x-intercepts

x = -b +- sqrt(b^2 - 4ac) / 2a
x = 3 +- sqrt(9 - 4*2*-1) / 4
x = 3 +- sqrt(9 - -8) / 4
x = 3 +- sqrt(17) / 4
x = 3 + sqrt(17) / 4

x <= 3-sqrt(17) / 4 or x >= 3 + sqrt(17) / 4

Solve:
abs(2x - 5) <= 3
abs(2x-5) - 3 <= 0
graph f(x) = abs(2x - 5) - 3

x intercepts of "abs(2x -5) - 3";
abs(2x-5) = 3
2x-5 = 3 or -3
2x-8 = 4; 2x = 2; x = 1
[1,4]

OTHER WAY OF DOING IT

f(x) = abs(2x - 5)
g(x) = 3

solve f(x) <= g(x)

2x-5 = 3 or 2x -5 = -3
x = 1 or x = 4

RULES OF ABSOLUTE VALUE INEQUALITIES DECOMPOSITION RULES:

1.
abs(u) <= a "means" -a <= u <= a
abs(u) >= a "means" u >= a or u <= -a

Ex:
abs(2x -5) <= -3
-3 <= 2x -5 <= 3
+5            +5
2 <= 2x <= 8
1 <= x <= 4

RATIONAL INEQUALITIES

(2x^2 + 1) / (x-3) >= 0; "2x^2 + 1 >= 1 for all x...cuz its a square...you can always divide inequalities by a positive number"
                         1(x-3) >= 0
                         x - 3 >= 0
                         x >= 3


(2x-1) / (x - 3) >= 0 "Numerator could be negative or the denominator could be negative"

Case 1:
x - 3 > 0; x > 3
2x >= 1
x >= 1/2

Case 2:
x - 3 < 0; 2x - 1 <= 0
2x <= 1
x <= 1/2

Sign chart

Split nominator and denominator into two equations; see when its true for either. That's the intersection.

Relearn definitions for concave up and concave down.

Solve:
1/x >= x
1/x - x >= 0


Case 1:
x > 0

Case 2:


Partition numbers divine the domain into those that make it positive or negative

3.1 Exponential functions

Can be made to look like

y = f(x) = a . b^x

a /= 0, b > 0 and 


Concave up means its like a bowl; growth rate is increasing
Average rate of change is proportional to the y value.

y = 2^x
Increasing
Domain : (-oo,oo)
Continuous
Range (0,oo)
y-intercept of 1
Horizontal asymptote of y = 0

lim   2^x = 0
x->-oo

lim   2^x = 00
x->oo

e^x; instantaneous rate of change at 0 is equal to 1; its in between 2 and 3

If b > 1, then there is exponential growth.

b = 1 + r, where r is the growth rate.

Ex. b = 2 = 1 + r; r = 1
so r = 1 = 100%

Ex. Investment $1 and your
investment doubles every 10 years.

Let y = A(t) = value of investment after t years.

t  | A
0  | $1
10 | $2
20 | $4
30 | $8

A = 2^d, where d = $ of decades
A = 2^(t/10) where t = # of years

t  | A
0  | 2^0 = 1

y = a * b^t
a = 1
b^t = 2^t/10
b = 2^1/10
b = 10rt(2)

Annual growth rate?

b = 1 + r

r = b - 1
  = b - 1
  = 2^(1/10) - 1 = 0.0717734 = 7.17734% growth per year

A colony of bacteria is
doubling every hour.
Write an equation for he amount of bacteria after t days.
Use A0 for initial quantity

h | q
0 | A0
1 | A0 * 2
2 | A0 * 2^2
3 | A0 * 2^3

d | q
0 | A0
1 | A0 * 2^24
2 | A0 * 2^48

Daily growth rate:
r = b - 1
  = 16,777,215
  = 1,677,721,500%

Ex. I give you the growth rate:
You invest $1000 at 1% growth rate per month.
How much do you have after t years?

r = 1% = 0.01
b = 1 + r = 1.01

A(t) = $1000*(1.01^(12t))
A(t) = 1000 *(1.01^12)
r = b - 1 = 0.126825

If 0 < b < 1, then this produces exponential decay

y = (1/2)^x = (2^-1)^x = 2^-x
Properties:
Continuous
Domain: All Real numbers
Range (0,oo)
y-int = 1
concave up (decay rate is increasing)
decreasing

lim   (1/2)^x = 0
x->oo

lim   (1/2)^x = oo
x->-oo

C-14 decays with a half-life of 5700 years

t | A
0 | A0
5700 | A0 * 1/2
11,400 | A0 * 1/4
n * 5700 | A0 * (1/2)n


Say you start with 100% = 1
After t years
P = (1/2)^(t/5700)

growth rate = -50%)
What is it every year?

b = (1/2)^1/5700 = .999878
r = b - 1 ~ -.000122; -0.01% per year

---
10/8/15
Logistic Equation
---

f(x) = 1/(1 + e^-x)

Domain: All real numbers
Range: (0, 1)

lim    e^-x = 0
x -> oo
horizontal aymptote of y = 0

lim    1/(e^-x) = 1
x -> oo 
horizontal asymptote of y = 1

lim    e^-x = oo
x -> -oo 

lim    1/(1 + e^-x) = 0
x -> -oo

1/(1 + e^-x): concave up when x is negative; concave down when x is positive

Example:

c * e^-x-5 = horizontal compressions

c * e^-x = horizontal shift

Invest $1 at 100% interest.
How much will you have in 1 year?
A = (1+1) = 2

Compound semi-annually
Growth rate 50% every 6 months.
A = (1+.5)^2 = 1.5^2 = 2.25
A = (1+.25)^4 = 1.25^4 = 2.4414
A = (1+.08333)^12

lim    (1 + 1/x)^x = e
x->oo

Deriving exponential function from two points
where
(-3,-7), (16,5)

-7 = a * b^-3
5 = a * b^16 / -7 a * b^-3

-5/7 = b^19
b = 19root(-5/7)

(-3,7)
(16,5)

7 = a * b^-3
5 = a *b^16 / 7 a * b^-3

b = 19root(5/7) = 98244681886

5 = a(0.98244)^16; a = 5/((0.98244)^16)

What is a log? Inverse of a ???

b^u = v === log.b.v = u

y = b^x
y = log.b.x


Properites of logs

log.b.1 = 0
log.b.b = 1
log.b.b^x = x
b^(log.b.x) = x
log.b.(r*s) = log.b.R + log.b.S = b^x * b^y = b^(x+y)
log.b.(r/s) = log.b.R - log.b.S
log.b.(r^c) = c . log.b.(r); (b^x)^c = b^(c*x)

a logarithm is an exponent
2^3 = 8
3 is the exponent; 8 is the power
log does the opposite
it takes in the power, and gives you the exponent

Expand ln(x^2z^5 / y^3) = ln((x^2*z^5)/(y^3)) = ln(x^2 * z^5) - ln(y^3)
                                              = lnx^2 + lnz^5 - ln(y^3)
                                              = 2lnx + 5lnz - 3lny


y = b^x, b > 1 exponential growth
y = log.b.x, b > 1 exponential decay

y = b^x, 1 > b > 0; asymptote y = 0
y = log.b.x, 1 > b > 0; asymptote x = 0

You can't take the log of any non-positive number

Example:

What is the domain of y = log.10.(x^2 - 3)

x^2 - 3 > 0
x^2 > 3
abs(x) > sqrt(3)

abs(x) = sqrt(3)
x = +-sqrt(3)

Domain:
x > sqrt(3)
x < -sqrt(3)

(-oo,-sqrt(3)U(sqrt(3),oo)

You can also graph it.

The real law of inequalities is you can only multiply with increasing functions

sqrt(2) > sqrt(.2) ; you can square both sides and conclude that its true

log(4 billion)
log (4 * 10^9)
log4 + log10^9
log4 + 9 ~~ 9.6

M = log(x)
M = magnitude
x = relative seismic wave heights (as a ration of the smallest detectable earth
quake 100 yrs ago)

Compare 7.3 and a 6.6 earthquake
7.3 = log(x1)
6.6 = log(x2)

log(x1/x2) = log(x1) - log(x2) = 0.7

x1/x2 = 10^.7 = 5.01

A 7.3 earthquare has 5.01 times the seismic wave heights of a 6.6

x2 = 5.5 wave height
x1 = aftershock's wave height
aftershock has 2 times

x1/x2 = 2

Difference in their magnitude is log2 = .3; Aftershock has magnitude 5.8




-}
