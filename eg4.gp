set terminal latex
set output "eg4.tex"
set format y "$%g$"
set format x "$%.2f$"
set title 'Exercise 5$'
set xlabel "This is the $x$ axis"
set ylabel "$\\tan(2x)$"
unset key
set xtics -pi, pi/4
plot [-pi:pi] [-1:1] tan(2x)
