\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{comment}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
    \usepackage{xltxtra,xunicode}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Mapping=tex-text,Scale=MatchLowercase}
  \newcommand{\euro}{€}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\ifxetex
  \usepackage[setpagesize=false, % page size defined by xetex
              unicode=false, % unicode breaks when used with xetex
              xetex]{hyperref}
\else
  \usepackage[unicode=true]{hyperref}
\fi
\usepackage[usenames,dvipsnames]{color}
\hypersetup{breaklinks=true,
            bookmarks=true,
            pdfauthor={},
            pdftitle={},
            colorlinks=true,
            citecolor=blue,
            urlcolor=blue,
            linkcolor=magenta,
            pdfborder={0 0 0}}
\urlstyle{same}  % don't use monospace font for urls
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}

\date{}

% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

\begin{document}

\section{Homework #7}\label{test}

Dear Meister Gladish,\\
\\
I figured that now would be as good of a time as any to finally learn how to use Latex.Thus, I present to you, my Latexified homework. Please excuse any gratuituous use of notation.\\
\\
Sincerely,\\
Mario J. Hesles

P.S. Graphs in $\LaTeX$ are hard! I linked you to graphs at desmos.com/calculator where the question asked to graph stuff.

\subsection{Chapter 3.1 Exercises}

\subsection{Ex. 1-7}
Which of the following are exponential functions? For those that are, state the initial value and the base. For those that are not, explain why not.
\subsubsection{Ex. 1}

\begin{itemize}
\tightlist
\item
  $y = x^8$
\item
  This is an example of a non-exponential function. All exponential functions must have a positive base. The preceding function \emph{could} have a positive base, but because the base is $x$, i.e.\ a variable, we do not have that guarantee. So, the function is not an exponential function.
\end{itemize}

\subsubsection{Ex. 2}
\begin{itemize}
\tightlist
\item
  $y = 3^x$
\item
  $a = 1$
\item
  $b = 3$
\end{itemize}

\subsubsection{Ex. 3}
\begin{itemize}
\tightlist
\item
  $y = 5^x$
\item
  $a = 1$
\item
  $b = 5$
\end{itemize}
\subsubsection{Ex. 4}
\begin{itemize}
\tightlist
\item
  $y = 4^2$
\item
  This is a constant function. A constant function is not an instance of an exponential function according to the authors. It is not an exponential functions.
\end{itemize}

\subsubsection{Ex. 5}
\begin{itemize}
\tightlist
\item
  $y = x^{\sqrt{x}}$
\item
This function is not guaranteed to have a positive base, and neither is it defined for all real numbers, as $\forall x ((x < 0) \to Undefined(\sqrt{x}))$. This is not an exponential function.
\end{itemize}

\subsubsection{Ex. 6}
\begin{itemize}
\tightlist
\item
  $y = x^{1.3}$
\item
  This is not a exponential function. An exponential function requires that the base by positive. This has a variable for the base, and so we don't have that guarantee.
\end{itemize}


\subsubsection{Ex. 7}
Compute the exact value of the function for the given x-value without using a calculator.
\begin{itemize}
\tightlist
\item
  $f(x) = 3 \cdot 5^x$ for $x = 0$
\item
  $f(0) = 3 \cdot 5^0$
\item
  $f(0) = 3 \cdot 1$
\item
  $f(0) = 3$
\end{itemize}

\subsection{Exercises 11 - 12}
Determine a formula for the exponential function whos values are given in Table 3.6.

\subsubsection{Ex. 11}

\begin{itemize}
\tightlist
\item
  $f(x)$
\item $\{(-2,6),(-1,3),(0,{\frac{3}{2}}),(1,{\frac{3}{4}}),(2,{\frac{3}{8}})\}$
\item $f(x) = a \cdot b^x$
\item Because $f(0) = \frac{3}{2}$ the initial value $a$ is $\frac{3}{2}$.
\item Because $f(1) = \frac{3}{2} \cdot b^1 = \frac{3}{4}$ the base $b$ is 
$\frac{1}{2}$. So,
\[f(x) = \frac{3}{2} \cdot ({\frac{1}{2}})^x\]
\end{itemize}

\subsubsection{Ex. 12}
\begin{itemize}
\tightlist
\item
  $g(x)$
\item 
  $\{(-2,108),(-1,36),(0,12),(1,4),(2,{\frac{4}{3}})\}$
\item 
  $g(x) = a \cdot b^x$
\item 
  Because $g(0) = 12$ the initial value $a$ is $12$.
\item 
  Because $g(1) = 12 \cdot b^1 = 4$ the base $b$ is $\frac{1}{3}$. So,
\[g(x) = 12 \cdot ({\frac{1}{3}})^x\]
\end{itemize}

\subsection{Ex. 13-14}
Determine a formula for the exponential function whose graph is shown. 
(Graphs in Latex are involved...people are suggesting I learn $R$, 
but I have Precalculus to do!)

\subsubsection{Ex. 13}
\begin{itemize}
\tightlist
\item
  $f(x) = a \cdot b^x$
\item
  $\{(0,3),(2,6)\}$
\item
  Because $f(0) = 3$ the initial value $a$ is $3$.
\item
  Because $f(2) = 3 \cdot b^2 = 6$ the base $b$ is $\sqrt{2}$. So,
\[f(x) = 3 \cdot ({\sqrt{2}})^x\]
\end{itemize}

\subsubsection{Ex. 14}
\begin{itemize}
\tightlist
\item
  $g(x) = a \cdot b^x$
\item
  $\{(0,2),(1,{\frac{2}{e}})\}$
\item
  Because $g(0) = 2$ the initial value $a$ is $2$.
\item
  Because $g(1) = 2 \cdot b^1 = \frac{2}{e}$ the base $b$ is $\frac{1}{e}$. So,
\[g(x) = 2 \cdot ({\frac{1}{e}})^x\]
\end{itemize}

\subsection{Ex. 15,18,19}
Describe how to transofrm the graph of $f$ into the graph of $g$. 
Sketch the graphs by hand and support your answer with a grapher.
\subsubsection{Ex. 15}
\begin{itemize}
\item
  $f(x) = 2^x$
\item
  $g(x) = 2^{(x-3)}$
\item
  Shift graph to the right by 3.
\item
  Graph: https://www.desmos.com/calculator/v7fm9ihv6q
\end{itemize}

\subsubsection{Ex. 18}
\begin{itemize}
\item
  $f(x) = 2^x$
\item
  $g(x) = 2^{5-x}$
\item
  Mirror across the y-axis.
\item
  Shift graph to the right by 5.
\item
  Graph: https://www.desmos.com/calculator/tua2iwjp4s
\end{itemize}

\subsubsection{Ex. 19}
\begin{itemize}
\item
  $f(x) = {0.5}^x$
\item
  $g(x) = 3 \cdot {0.5}^x + 4$
\item
  Vertically stretch by a factor of three.
\item
  Shift up by 4.
\item
  Graph: https://www.desmos.com/calculator/uhfnxivmxy
\end{itemize}

\subsection{Ex. 35}
Solve the inequality graphically.
\begin{itemize}
\item
  Graphs: https://www.desmos.com/calculator/xkidfugnym
\item
  For all $x < 0$, $9^x < 4^x$.
\end{itemize}

\subsection{Ex. 37}
\begin{itemize}
\item
  Graphs: https://www.desmos.com/calculator/knnl09mysz
\item
  For all $x < 0$, $({\frac{1}{4}})^x > ({\frac{1}{3}})^x$.
\end{itemize}

\subsection{Ex. 41}
Find the y-intercept and horizontal asymptotes with a grapher.
\begin{itemize}
\item
  $f(x) = \frac{12}{1 + 2 \cdot {0.8}^x}$
\item
  The y-intercept is $y = 5$. The horizontal asymptotes are at $y = 0$ and $y = 12$.
\end{itemize}

\subsection{Ex. 49}
Graph the function. Find  domain, range, continuity, increasing or decreasing behavior, symmetry, boundedness, extrema, asymptotes, and end behavior.
\begin{itemize}
\item
  $f(x) = \frac{5}{1 + 4 \cdot e^{-2x}}$
\item
  Domain: All real numbers. (Can't get pretty R to render correctly.)
\item
  Range: (0,5)
\item
  Continuity: Everywhere continuous.
\item
  Increasing over entire domain.
\item
  Symmetric vertically around $x = 0.693$.
\item
  Boundedness: Lower bound: 0 ; Upper bound: 5
\item
  Extrema: none.
\item
  Asymptotes: Horizontal asymptotes at $y = 5$ and $y = 0$.
\item
  End behavior: $\displaystyle \lim_{x\to \infty} f(x) = 5$
\item
  End behavior: $\displaystyle \lim_{x\to -{\infty}} f(x) = 0$
\end{itemize}

\subsection{Ex. 52}
When will the population of Columbus be $800,000$ if it was $632,910$ in $1990$
and $711265$ in $2000$ and the population increases exponentially?
\begin{itemize}
\item Because $f(0) = 632,910$ the initial value $a$ is $632,910$.
\item Because $f(10) = 632,910 \cdot b^{10} = 711,265$, $b$ is $1.0117400641750989$
\item So, $f(x) = 632,910 \cdot {1.01174}^x$
\item So, $800,000 = 632,910 \cdot {1.01174}^x$
\item $1.26400278080612 = {1.01174}^x$
\item $x = 20.072$
\item The year when the population of Columbus will be $800,000$ is $2010.072$
\end{itemize}

\subsection{Ex. 53}
When were the population of Columbus and the population of Austin equal?
\begin{itemize}
\item First we must derive the appropriate equation for Austin.
\item Because $f(0) = 465,622$ the initial value $a$ is $465,622$
\item Because $f(10) = 465,622 \cdot b^{10} = 656,562$, $b$ is $1.0349615728365753$
\item So, $f(x) = 465622 \cdot {1.03496}^x$
\item After consulting our grapher, it looks like 13.5275
\end{itemize}

\subsection{Ex. 57 58}
\subsection{Ex. 57}
The number of bacteria $B$ in a petri dish is given by the following:
\[B = 100 \cdot e^{0.693t}\]
\begin{itemize}
\item
  (a) The number of initial bacteria in the petri dish was 100, because $a$ is $100$.
\item
  (b) $B = 100 \cdot e^{0.693 \cdot 6}$ where $B = 100 \cdot e^{0.694 \cdot 6} = 6432$ is the number of bacteria after 6 hours.
\end{itemize}

\subsection{Ex. 58}
\begin{itemize}
\item
  (a) The initial amount of Carbon 14 is $20g$ because $a = 20$.
\item
  (b) The amount left after $10,400$ years is $5.646818g$.
\end{itemize}



\subsection{3.2 1,3,7-23,35,37,38,39,33}
\subsection{Ex. 1}
Tell whether the function is an exponential growth function or an exponential
decay function, and find the constant percentage rate of growth or decay.
\begin{itemize}
\item
  $P(t) = 3.6 \cdot 1.09^t$
\item
  This is an exponential growth function.
\item
  It's rate of growth is 9\%.
\end{itemize}

\subsection{Ex. 3}
\begin{itemize}
\item
  $f(x) = 78,963 \cdot 0.968^x$
\item
  This is an exponential decay function.
\item
  The rate of decay is 3.2\%.
\end{itemize}

\subsection{Ex. 7}
Determine the exponential function that satisfies the given conditions.
\begin{itemize}
\item
  Initial value $= 5$, increasing at a rate of $17\%$ per year.
\item
  $f(x) = 5 \cdot 1.17^x$
\end{itemize}

\subsection{Ex. 8}

\begin{itemize}
\item Initial value $= 52$, increasing at a rate of $2.3\%$ per day.
\item $f(x) = 52 \cdot 1.023^x$
\end{itemize}

\subsection{Ex. 9}
\begin{itemize}
\item Initial value $= 16$, decreasing at a rate of $50\%$ per month.
\item $f(x) = 16 \cdot 0.5^x$
\end{itemize}

\subsection{Ex. 10}
\begin{itemize}
\item Initial value $= 5$, decreasing at a rate of $0.59\%$ per week.
\item $f(x) = 5 \cdot .941^x$
\end{itemize}

\subsection{Ex. 11}
\begin{itemize}
\item Initial value $= 28,900$, decreasing at a rate of $2.6\%$ per year.
\item $f(x) = 28,900 \cdot 97.4^x$
\end{itemize}

\subsection{Ex. 12}
\begin{itemize}
\item Initial value $= 502,000$, increasing at a rate of $1.7\%$ per year.
\item $f(x) = 502,000 \cdot 1.017^x$
\end{itemize}

\subsection{Ex. 13}
\begin{itemize}
\item Initial value $= 18$cm, increasing at a rate of $5.2\%$ per week.
\item $f(x) = 18 \cdot 1.052^x$
\end{itemize}

\subsection{Ex. 14}

\begin{itemize}
\item Initial value $= 15$g, decreasing at a rate of $1.7\%$ per year.
\item $f(x) = 15 \cdot 0.983^x$
\end{itemize}

\subsection{Ex. 15}

\begin{itemize}
\item Initial value $= 0.6$g, doubling every three days.
\item $f(x) = 0.6 \cdot 2^x/3$
\end{itemize}

\subsection{Ex. 16}
\begin{itemize}
\item Initial value $= 250$, doubling every $7.5$ hours.
\item $f(x) = 250 \cdot 2^x/(7.5/24)$
\end{itemize}

\subsection{Ex. 17}

\begin{itemize}
\item Initial value $= 592$g, halving once every $6$ hours.
\item $f(x) = 592 \cdot .5^x/(6/24)$
\end{itemize}

\subsection{Ex. 18}

\begin{itemize}
\item Initial value $= 17$g, halving once every $7.5$ hours.
\item $f(x) = 17 \cdot 2^x/(32/24)$
\end{itemize}

\subsection{Ex. 19}
Look at table and give equation.
\begin{itemize}
\item Because $f(0) = 2.3$, $a = 2.3$.
\item Because $f(1) = 2.3 \cdot b^1 = 2.875$, $b = \frac{2.875}{2.3} = 1.25$
\item So, $f(x) = 2.3 \cdot 1.25^x$
\end{itemize}

\subsection{Ex. 20}
Look at table and give equation.
\begin{itemize}
\item Because $g(0) = -5.8$, $a = -5.8$.
\item Because $g(1) = -5.8 \cdot b^1 = -4.64$, $b = \frac{-4.64}{-5.8} = 0.8$
\item So, $g(x) = 2.3 \cdot 0.8^x$
\end{itemize}

\subsection{Ex. 21}
Look at graph and give equation.
\begin{itemize}
\item Because $f(0) = 4$, $a = 4$.
\item Because $f(1) = 4 \cdot b^5 = 8.05$, $b^5 = \frac{8.05}{4}$, $b = 1.150131$
\item Thus, $f(x) = 4 \cdot 1.150131^x$
\end{itemize}

\subsection{Ex. 22}
Look at graph and give equation.
\begin{itemize}
\item Because $f(0) = 3$, $a = 3$.
\item Because $f(4) = 3 \cdot b^4 = 1.49$, $b^4 = \frac{1.49}{3}$, $b = 0.839491$
\item Thus, $f(x) = 3 \cdot 0.839491^x$
\end{itemize}

\subsection{Ex. 23}
Find the logistic function that satisfies the given conditions.
\begin{itemize}
\item Initial value $= 10$, limit to growth $= 40$, passing through $(1,20)$.
\item $\frac{40}{1 + 10 \cdot 20^x}$
\end{itemize}

\subsection{Ex. 33}
Radioactive decay: The half-life of a certain radioactive substance is 14 days. There are $6.6$g present initially.
\begin{itemize}
\item $(a)$ Express the amount of substance remaining as a function of time $t$. Alright, $f(x) = 6.6 \cdot {\frac{1}{2}}^{\frac{t}{14}}$
\item $(b)$ When will there be less than $1$g remaining? I'm going to cheat by graphing it, and looking at where the $y$ value is $1$. Ok, $38.1146$ days.
\end{itemize}

\subsection{Ex. 35}
Without using formulas or graphs, compare and contrast exponential and linear functions.
\begin{itemize}
\item The rate of change for a given linear function at any particular point is zero. The rate of change for a given exponential function at any particular point is either increasing, for an exponential growth function, or decreasing, for an exponential decay function.
\item Linear functions are continuous across their domains. So are exponential functions.
\item Linear functions and exponential functions are defined across their domains.
\item Linear functions are neither concave up or concave down. Exponential functions are either concave up or concave down.
\end{itemize}

\subsection{Ex. 37}
\begin{itemize}
\item The reason why the time it takes the population to double is independent of the population size, is because the population size is the dependent variable. The independent variable is time. This is the case because its an exponential growth function. For any given initial population (of at least two adults capable of procreating successfully), given enough time, it will reach what population is desired.
\end{itemize}

\subsection{Ex. 38}
\begin{itemize}
\item The reason why the half-life of a radioactive substance is independent of the initial amount of the substance present, is because its dependent upon the particular physical make-up of a particular atom and the amount of time that passes. Past a certain threshold, given enough time, the liklihood of a particular atom decaying reaches $1.00$. The half-life is intrinsic to the atom itself, and not how many total atoms are present.
\end{itemize}

\subsection{Ex. 39}
The number of bacteria in a petri dish culture after $t$ hours is given by
\[B = 100 \cdot e^{0.693t}\]\\
When will the number of bacteria be $200$? Estimate the doubling time of the bacteria.
\begin{itemize}
\item The number of bacteria will be $200$ after $1.00021$ hours.
\item The doubling time of the bacteria is roughly $1$ hour.
\end{itemize}

\subsection{3.3 1-3,5,6,8,12,13,18,19,21,23,33,35,37,38,39,40,41,43,51,52,57,59,61}

\subsection{Ex. 1}
\begin{itemize}
\item $\log_{4}4 = 1$
\end{itemize}

\subsection{Ex. 2}
\begin{itemize}
\item $\log_{6}1 = 0$
\end{itemize}

\subsection{Ex. 3}
\begin{itemize}
\item $\log_{2}32 = 5$
\end{itemize}

\subsection{Ex. 5}
\begin{itemize}
\item $\log_{5}{\sqrt[3]{25}} == 5^x = \sqrt[3]{25} == 5^{2} \cdot 5^{\frac{1}{3}} == x = \frac{2}{3}$
\end{itemize}

\subsection{Ex. 6}
\begin{itemize}
\item $\log_{6}{\frac{1}{\sqrt[5]{36}}} == $
\item $6^x = \frac{1}{\sqrt[5]{36}} == $
\item $6^x = 36^{-{\frac{1}{5}}} == $
\item $6^x = 6^2 \cdot 6^{-{\frac{1}{5}}} == $
\item $x = {-{\frac{2}{5}}}$
\end{itemize}

\subsection{Ex. 8}
\begin{itemize}
\item $\log_{10}{10000} = 4$
\end{itemize}

\subsection{Ex. 12}
\begin{itemize}
\item $\log_{10}{\frac{1}{\sqrt{1000}}} = -{\frac{3}{2}}$
\end{itemize}

\subsection{Ex. 13}
\begin{itemize}
\item $ln(e^3) = 3$
\end{itemize}

\subsection{Ex. 18}
\begin{itemize}
\item $ln(\frac{1}{\sqrt{e^7}}) = -{\frac{7}{2}}$
\end{itemize}

\subsection{Ex. 19}
Evaluate without a calculator
\begin{itemize}
\item $7^{\log_{7}{3}} = 3$ So, ${\log_{7}{3}}$ is the number that you need to multiply $7$ by to get $3$, which, if you raise $7$ to, equals $3$.
\end{itemize}

\subsection{Ex. 21}
\begin{itemize}
\item $10^{\log_{10}{0.5}} = 0.5$
\end{itemize}

\subsection{Ex. 23}
\begin{itemize}
\item $e^{\ln(6)} = 6$
\end{itemize}

\subsection{Ex. 33}
Solve by changing to exponential form.
\begin{itemize}
\item $\log_{10}{x} = 2$
\item $10^{2} = 100$
\end{itemize}

\subsection{Ex. 35}
\begin{itemize}
\item $\log_{10}{x} = -1$
\item $10^{-1} = x$
\item $x = \frac{1}{10}$
\end{itemize}

\subsection{Ex. 37}
Match the function with its graph.
\begin{itemize}
\item $f(x) = \log_{10}{1 - x}$
\item The graph if $(d)$
\end{itemize}

\subsection{Ex. 38}
\begin{itemize}
\item $f(x) = \log_{10}{x + 1}$
\item The graph is $(b)$.
\end{itemize}

\subsection{Ex. 39}
\begin{itemize}
\item $f(x) = -\ln(x - 3)$
\item The graph is $(c)$.
\end{itemize}

\subsection{Ex. 40}
\begin{itemize}
\item The graph is $(a)$.
\end{itemize}

\subsection{Ex. 41}

\begin{itemize}
\item $f(x) = \ln(x + 3)$
\item Move the graph to the left by 3.
\item https://www.desmos.com/calculator/zmzdwosrkr
\end{itemize}

\subsection{Ex. 43}
\begin{itemize}
\item $f(x) = \ln(-x) + 3$
\item Mirror the graph across the y axis.
\item Shift the graph up by 3.
\item Graph: https://www.desmos.com/calculator/rrxoloyzqm
\end{itemize}

\subsection{Ex. 51}
\begin{itemize}
\item $f(x) = 2 \cdot \log_{10}{3 - x} - 1$
\item Mirror the graph across the y axis.
\item Shift the graph to the right by 3.
\item Stretch the graph vertically by a factor of 2.
\item Shift the graph down by 1.
\item Graph: https://www.desmos.com/calculator/podukj2byl
\end{itemize}

\subsection{Ex. 52}
\begin{itemize}
\item $f(x) = -3 \cdot \log_{10}{1 - x} + 1$
\item Mirror the graph across the x axis.
\item Shift the graph to the right y $1$.
\item Stretch the graph vertically by a factor of 3
\item Mirror the graph across the x axis.
\item Shift the graph up by $1$.
\item Graph: https://www.desmos.com/calculator/8ofqbs1pqr
\end{itemize}

\subsection{Ex. 57,59,61}
\subsection{Ex. 57}
Graph the function, and analyze it for domain, range, continuity, increasing or decreasing behavior, boundedness, extrema, symmetry, asymptotes, and end behavior.
\begin{itemize}
\item $f(x) = 3 \cdot \log(x) - 1$
\item Domain: $(0,\infty)$
\item Range: $({-\infty},{\infty}$
\item Continuity: Continuous everywhere on domain.
\item Increasing: Across all domain.
\item Decreasing: Nowhere.
\item Bounded: Nowhere.
\item No extrema.
\item Not symmetric.
\item Vertical asymptote: x = 0 
\item Horizontal asymptote: n/a
\item $\lim_{x\to \infty} f(x) = \infty$.
\item $\lim_{x\to 0}^+ f(x) = -{\infty}$.
\item Graph: https://www.desmos.com/calculator/beuvkggqmh
\end{itemize}

\subsection{Ex. 59}
$\beta = 10 \cdot \log_{10}{\frac{I}{I_0}}$ where $I_0 = 10^{-12} \frac{W}{m^2}.$
\begin{itemize}
\item Soft whisper at $5$m is, $\beta = 10 \cdot \log_{10}{\frac{10^{-11}}{10^{-12}}} = 10 \cdot log_{10}{10} = 10 \cdot 1 = 10$
\item City traffic is, $\beta = 10 \cdot \log_{10}{\frac{10^{-5}}{10^{-12}}} = 10 \cdot \log_{10}{10^7} = 10 \cdot 7 = 70$
\item A jet at takeoff is, $\beta = 10 \cdot \log_{10}{\frac{10^{3}}{10^{-12}}} = 10 \cdot \log_{10}{10^15} = 10 \cdot 15 = 150$
\end{itemize}

\subsection{Ex. 61}
\begin{itemize}
\item Around the year $2023$, the population of San Antonio will be roughly $1,151,305$, if the future follows the past.
\end{itemize}

\subsection{3.4 1,7,13,15,17,19,21,29,39,51,52,53}

\subsubsection{Ex. 1}
\begin{itemize}
\item $\ln(8x) = \ln(8) + \ln(x)$
\end{itemize}

\subsubsection{Ex. 7}
\begin{itemize}
\item $\log_{10}{{x^3}{y^2}} = 3 \cdot \log_{10}{x} + 2 \cdot \log_{10}{y}$
\end{itemize}

\subsubsection{Ex. 13}
\begin{itemize}
\item $\log_{10}{xy}$
\end{itemize}

\subsubsection{Ex. 15}
\begin{itemize}
\item $\ln(\frac{y}{3})$
\end{itemize}

\subsubsection{Ex. 17}
\begin{itemize}
\item $\log_{10}{\sqrt[3]{x}}$
\end{itemize}

\subsubsection{Ex. 19}
\begin{itemize}
\item $\ln({x^2}{y^3})$
\end{itemize}

\subsubsection{Ex. 21}
\begin{itemize}
\item $\log_{10}{\frac{{xy}^4}{{yz}^3}}$
\end{itemize}

\subsubsection{Ex. 29}
This is just a reminder for myself: $\log_{b}{x} = \frac{\log_{a}x}{log_{a}b}$//
Write the expression using only natural logarithms. $\log_{10}x = \frac{\ln(x)}{\ln(10)}$
\begin{itemize}
\item $\log_{3}x = \frac{\ln(x)}{\ln(3)}$
\end{itemize}

\subsubsection{Ex. 39}
Describe how to transform the graph of $g(x) = \ln(x)$ into the graph of the given function. Sketch the graph by hand and support with a grapher.
\begin{itemize}
\item $f(x) = log_{4}x$
\item Because $f(x) = log_{4}x = \frac{\ln(x)}{\ln(4)}$, it's graph is obtained by vertically shrinking the graph of $f(x) = \ln(x)$ by a factor of $\frac{1}{\ln(4)}$ or around $0.7213$.
\item Graph: https://www.desmos.com/calculator/rs4qqeiqpy
\end{itemize}

\subsubsection{Ex. 51}
\begin{itemize}
\item $(a) = 10 \cdot \log_{10}{\frac{10^{-12}}{10^{-12}}} = 10 \cdot \log_{10}{1} = 10 \cdot 0 = 0$
\item $(b) = 10 \cdot \log_{10}{\frac{10^{-11}}{10^{-12}}} = 10 \cdot \log_{10}{10} = 10 \cdot 1 = 10$
\item $(c) = 10 \cdot \log_{10}{\frac{10^{-6}}{10^{-12}}} = 10 \cdot \log_{10}{10^6} = 10 \cdot 6 = 60$
\item $(d) = 10 \cdot \log_{10}{\frac{10^{-4}}{10^{-12}}} = 10 \cdot \log_{10}{10^8} = 10 \cdot 8 = 80$
\item $(e) = 10 \cdot \log_{10}{\frac{10^{-2}}{10^{-12}}} = 10 \cdot \log_{10}{10^10} = 10 \cdot 10 = 100$
\item $(e) = 10 \cdot \log_{10}{\frac{10^0}{10^{-12}}} = 10 \cdot \log_{10}{10^12} = 10 \cdot 12 = 120$
\end{itemize}

\subsubsection{Ex. 52}
\begin{itemize}
\item $(a) == R = \log_{10}{\frac{250}{2}} + 4.25 == R = 2.09691001301 + 4.25 == R = 6.34691001301$
\item $(b) == R = \log_{10}{\frac{300}{4}} + 3.5 == R = 1.87506126339 + 3.5 == R = 5.37506126339$
\end{itemize}


\subsubsection{Ex. 53}
\begin{itemize}
\item $\log_{10}{\frac{I}{12}} = -0.00235 \cdot 40 == \log_{10}{\frac{I}{12}} = -0.094$
\item $10^{-0.094} = {\frac{I}{12}} == 10^{-0.094} \cdot 12 = I$
\item $I = 9.66454129439$
\end{itemize}







\subsection{What follow is classwork}
\subsection{Log examples}
\begin{itemize}
\item $log_{b}1 = 0$
\item $log_{b}b = 1$
\item $log_{b}{b^R} = R$
\item $b^{log_{b}R} = R$
\item $log_{b}{RS} = log_{b}R + log{b}S$
\item $log_{b}{\frac{R}{S}} = log_{b}R - log_{b}S$
\item $log_{b}{S^R} = R \cdot log_{b}S$
\item ${b^x} \cdot b^y = b^{x+y}$
\item $\frac{b^x}{b^y} = b^{x-y}$
\item $(b^{x})^{R} = b^{R \cdot x}$
\end{itemize}

\subsection{More log examples}
\begin{itemize}
\item $log_{10}{\frac{I}{12}} = {-0.00235(40)}$
%\item $log_{10}{\frac{I}{12} = {-0.094} \to log_{10}{I} - log_{10}{12} = {-0.094}$
\item $\frac{I}{12} = 10^{-0.094}$
\end{itemize}

\subsubsection{New}
\begin{itemize}
\item $log_{10}{x} + log_{2}{x+3} = 16$
\item $log{2}{x(x+3)} = 2$
\item $x(x+3) = 2^2 = 4$
n\item $x^2 + 3x = 4$
\item $x^2 +3x - 4 = 0$
\item $(x+4)(x-1) = 0$
\item Check $log_{2}1 + log_{2}4 = 0 + 2 = 2$
\end{itemize}

\subsubsection{Next example}
\begin{itemize}
\item $log_{b}x = \frac{log_{a}x}{log_{a}}b$
\item $log_{b}x = y$
\item $b^y = x$
\item $log_{a}{b^y} = log_{a}x$
\item $y \cdot log_{a}b = log_{a}x$
\item $y = \frac{log_{a}x}{log_{a}x} = log_{b}x$
\end{itemize}

\subsubsection{Next Example}
\begin{itemize}
\item You have $\$100$ invested in a fund that earns $1.5\%$ each quarter. How long in years + months) will it take for the investment to be worth $\$1200$?
\item Growth rate $4 = .015$ each quarter.
\item base $b = 1 + 4 = 1.015$
\item Value after $x$ quarters?
\item That is, $V = (1000) \cdot (1.015^x)$
\item Find $x$ when $V$ is $\$1200$
\item $1200 = 1000 \cdot {1.015^x}$
\item $1.2 = 1.015^x$
\item Or $\ln{1.2} = \ln{1.015^x}$
\item $\ln{1.2} = x \cdot \ln{1.015}$

\item How long to $1400$
\item $1.4 = 1.015^x$
\item $x = \frac{log_{10}{1.4}}{log_{10}{1.015}} = 22.6$
\item $23$ quarters.
\item $5$ yrs, 9 months.

\item New.
\item Newton's law of cooling.
\item $T(t) = T_m + (T_0 - T_m)\cdot e^{-kt}$ where $T_m$ is the ambient temperature.
\item $T_o$ is the initial temperature.
\item $k$ is a constant determined whatever is cooling.

\item New.
\item Soup is boiling and is removed from the stove and is set to cool in a room
whose temperature is $76^{\circ}$.
\item After $1$ minutes, the temperature of the soup is $126^{\circ}$.
\item How long will it take until the soup is $100^{\circ}$?
\item $T_m = 76^{\circ}$
\item $T_o = 212^{\circ}$
\item $T(t) = 76^{\circ} + 136 \cdot e^{-kt}$
\item When $t = 1, T = 126$
\item $126 = 76 + 136 \cdot e^{-k}$
\item $-76 -76$
\item $50 = 136 \cdot e^-k$
\item $\frac{50}{136} = e^-k$
\item $e^k = \frac{136}{50} - 2.72$
\item $k = ln(2.72 = 1.006$
\item $T(t) = 76 + 136 \cdot e^{-1.0006t}$
\item $100 = 76 + 136 \cdot e^{-1.0006t}$
\item $24 = 136 \cdot e^{-1.0006t}$
\item $\frac{24}{136} = e^{-1.0006t}$
\item Or you could do $e^{-1.0006t} = \frac{24}{136}$
\item ${-1.0006t} = ln({\frac{24}{136}})$
\item $\frac{ln(\frac{24}{136})}{-1.0006}$
\item New
\item $ln(\frac{a}{b}) = ln(a) - ln(b) = -(ln(b) - ln(a)) = -{ln(\frac{a}{b})}$
\item Problem on homework.
\item Richter Scale
\item $M = log_{10}x$ where $x$ is intensity relative to reference Earthquake
\item Decibels
\item $dB = 10 \cdot log_{10}x$ where $x$ is intensity relative to reference sounds
\item p stands for $-log$ in pH is $-log_{10}{H^+}$
\item ph $= 7.0$ ; $H^+ = 10^{-7.0}$
\item pH $= -5.3$
\item $H^+ = 10^5.3$ ; $\frac{10^5.3}{10^-{7.0}} = 10^{12.3}$
\item $7.0 - (-5.3) = 12.3$
graphix padowan ... lindwedgraphics

\end{itemize}

\end{document}
